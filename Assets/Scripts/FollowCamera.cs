﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public Transform m_Player;
    private Vector3 m_Offset;

    private void Start() {
        m_Offset = m_Player.position - transform.position;
    }

    void Update()
    {
        transform.position = m_Player.position - m_Offset;
    }
}
