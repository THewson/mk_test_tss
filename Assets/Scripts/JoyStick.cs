﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class JoyStick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {
    public Image m_BottomImage;
    public Image m_TopImage;
    public Vector3 m_InputVector;
    public bool m_IsActive = false;

    public void OnDrag(PointerEventData eventData) {
        // If pointer is on the image
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
                                         m_BottomImage.rectTransform, eventData.position,
                                         eventData.pressEventCamera, out Vector2 pos)) {
            m_IsActive = true;

            // Returns a 0 -> 1 Value across whole image
            pos.x = pos.x / m_BottomImage.rectTransform.sizeDelta.x;
            pos.y = pos.y / m_BottomImage.rectTransform.sizeDelta.y;

            // Centres the 0 -> 1 to the centre of the joystick
            float x = (m_BottomImage.rectTransform.pivot.x == 1) ? pos.x * 2 + 1 : pos.x * 2 - 1;
            float y = (m_BottomImage.rectTransform.pivot.y == 1) ? pos.y * 2 + 1 : pos.y * 2 - 1;

            m_InputVector = new Vector3(x, 0, y);

            // Keeps topimage from moving in a square
            m_InputVector = (m_InputVector.magnitude > 1) ? m_InputVector.normalized : m_InputVector;

            // Applies the changes to the top of the joystick
            m_TopImage.rectTransform.anchoredPosition = new Vector3(
                m_InputVector.x * (m_BottomImage.rectTransform.sizeDelta.x / 3.5f),
                m_InputVector.z * (m_BottomImage.rectTransform.sizeDelta.y / 3.5f));
        }
    }

    public void OnPointerDown(PointerEventData eventData) {
        OnDrag(eventData);
    }

    public void OnPointerUp(PointerEventData eventData) {
        m_InputVector = Vector3.zero;
        m_TopImage.rectTransform.anchoredPosition = m_InputVector;
        m_IsActive = false;
    }

    public float GetHorizontal() {
        if (m_InputVector.x != 0)
            return m_InputVector.x;
        else
            return Input.GetAxis("Horizontal");
    }

    public float GetVertical() {
        if (m_InputVector.z != 0)
            return m_InputVector.z;
        else
            return Input.GetAxis("Vertical");
    }
}
