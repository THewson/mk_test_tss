﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Main Player Controller
public class PlayerController : MonoBehaviour {
    public JoyStick m_JoyStickMovement, m_JoyStickGun;
    public Image m_JoyStickMovement_IMG, m_JoyStickGun_IMG;
    public GameObject m_Projectile;

    public float m_ProjectileSpeed = 100;
    public float m_FireRate = 0.5f;
    private float m_RechargeTime = 0;

    // Start is called before the first frame update
    void Start() {
        m_JoyStickGun.m_BottomImage = m_JoyStickGun_IMG;
        m_JoyStickGun.m_TopImage = m_JoyStickGun_IMG.transform.GetChild(0).GetComponent<Image>();

        m_JoyStickMovement.m_BottomImage = m_JoyStickMovement_IMG;
        m_JoyStickMovement.m_TopImage = m_JoyStickMovement.transform.GetChild(0).GetComponent<Image>();
    }
    // Calculates a direction in respect to the camera
    public Vector3 NewFacingDirection(float vertAxis, float hozAxis) {
        Camera cam = Camera.main;
        
        Vector3 right = cam.transform.right;
        Vector3 forward = cam.transform.forward;

        forward.y = 0f;
        right.y = 0f;
        forward.Normalize();
        right.Normalize();

        var newrotation = forward * vertAxis + right * hozAxis;

        return newrotation;
    }

    // Update is called once per frame
    void FixedUpdate() {
        // Recharge Timer
        if (m_RechargeTime >= 0)
            m_RechargeTime -= Time.deltaTime;
        
        // Gun
        if (m_JoyStickGun.m_IsActive) {
            float hozAxis = m_JoyStickGun.GetHorizontal();
            float vertAxis = m_JoyStickGun.GetVertical();

            // Projectile code
            if (m_RechargeTime <= 0) {
                m_RechargeTime = m_FireRate;
                GameObject pro = Instantiate<GameObject>(m_Projectile, transform.position + transform.forward * 0.3f, transform.rotation);
                pro.GetComponent<Rigidbody>().AddForce(transform.forward * m_ProjectileSpeed);
            }
            // Faces the player towards the sticks direction
            Vector3 facingdirection = NewFacingDirection(vertAxis, hozAxis);
            if (facingdirection != Vector3.zero) {
                transform.forward = facingdirection;
            }
        }

        // Movement
        if (m_JoyStickMovement.m_IsActive) {
            float hozAxis = m_JoyStickMovement.GetHorizontal();
            float vertAxis = m_JoyStickMovement.GetVertical();

            Vector3 facingdirection = NewFacingDirection(vertAxis, hozAxis);

            // When only the movement stick is active
            if (facingdirection != Vector3.zero && !m_JoyStickGun.m_IsActive) {
                transform.position += m_JoyStickMovement.m_InputVector * Time.deltaTime;
                transform.forward = facingdirection;
            }
            // In the case that both of the sticks are being moved
            else if (facingdirection != Vector3.zero && m_JoyStickGun.m_IsActive) {
                transform.position += m_JoyStickMovement.m_InputVector * Time.deltaTime;
            }
        }
    }
}
