﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float m_Lifetime = 4.0f;
    public Rigidbody m_Rb;

    // Update is called once per frame
    void Update()
    {
        m_Lifetime -= Time.deltaTime;
        if(m_Lifetime < 0) {
            Destroy(this.gameObject);
        }

    }
}
